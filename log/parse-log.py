#!/usr/bin/python
#
# Parse bts-link logs file to generate graph of evolution
#
# Author: Sandro Tosi
# Copyright: 2009 Sandro Tosi <morph@debian.org>
# License: Public Domain
#
# It generated 2 graphs:
#   1. a general evolution of all teh logs available
#   2. only the unmatched for the latest log, group by package
#
# Generation of graph 1 is quite complex, becuase I want it to be
# flexible enough to react to tags addition without having to touche
# the source code.
#
# Graph 2 contains histograms for the packages with
# unmatched/unconfigured remote BTS. There is a minimum count limit to
# go into the graph (otherwise too much 1 bug entry will come). We
# should address those with higher count first (much gain)

# for file opening
from __future__ import with_statement
# to list log files
import glob
# for regexp support
import re
# to parse date from log file names
import dateutil.parser
# matplotlib imports, for plotting and date handling
import matplotlib.pyplot as plt
import matplotlib.dates
# for numpy arange function
import numpy as np


# minimum count for package in unconfigured to comes up on the graph
U_MIN = 3


# regexp for date in log file name
logdate_re = re.compile(r'log_(.*)')
# regexp for tag in log lines: <tag>: <rest of line>
log_tags = re.compile(r'^(.): .*')
# regexp for package extraction form unmatched/unconfigured lines
unmatched = re.compile(r'^U:.*pkg=(.*), bug=.*')


###### GENERAL GRAPH CODE ######

# get file matching log_*
files = glob.glob('log_*')
# sort the list
files = sorted(files)

# will contain the tags for each logs
parsed = {}
# temporary tags list
tmp_tags = []

# loop over files
for file in files:
    # generate the date from the log file name (it's YYYY-MM-DD)
    logdate = logdate_re.match(file).group(1)
    # temporary dict to contain tags + tags count for current file
    tmp = {}
    with open(file) as f:
        # for every line in the file
        for line in f:
            m = log_tags.match(line)
            if m:
                k = m.group(1)
                # for current tag, increment or add
                if k in tmp.keys():
                    tmp[k] = tmp[k] + 1
                else:
                    tmp[k] = 1
        # add all the current tags to temporary list
        tmp_tags += tmp.keys()
        # add tag dictionary to general dict for parsed dic
        parsed[logdate] = tmp


# generate a uniq list (sorted) of all the tags available in all the log files
tags = sorted(dict.fromkeys(tmp_tags).keys())

# compute the latest log date available
# the last element of the sorted list of keys in parsed dict
latest_log = sorted(parsed.keys())[-1]

# will contain the lists with data for each tag, ordered by date
data = {}

# initialize the dict, for all the tags, with an empty list
for tag in tags:
    data[tag] = []

# for all the (in a sorted way, so lists are ordered by date)
for log in sorted(parsed.keys()):
    # and for all the tags available across all logs
    for tag in tags:
        # if the log contains the tag
        if tag in parsed[log].keys():
            # add the tag count to the tag list
            data[tag].append(parsed[log][tag])
        else:
            # else add None, so no point is drawn
            data[tag].append(None)


###### UNMATCHED/UNCONFIGURED GRAPH CODE ######

# package dictionary
pkgs = {}
# selected packages, those >= U_MIN
display_pkgs = {}

# get the most recent log file from the (sorted) files list
with open(files[-1]) as f:
    for line in f:
        m = unmatched.match(line)
        if m:
            pkg = m.group(1)
            # add package to count or initialize it
            if pkg in pkgs:
                pkgs[pkg] = pkgs[pkg] + 1
            else:
                pkgs[pkg] = 1

# filter packages
for key in pkgs.keys():
    if pkgs[key] >= U_MIN:
        display_pkgs[key] = pkgs[key]

# sort the dictionary by value (and secondly by key)
# http://coreygoldberg.blogspot.com/2008/06/python-sort-dictionary-by-values.html
sorted_pkgs = sorted(display_pkgs.items(), key=lambda(k,v):(v,k))


###### GRAPHING CODE ######


## first figure: general
# figure bigger than default 
fig1 = plt.figure(figsize=(9,6), dpi=100)
ax1 = fig1.add_subplot(111)

# convert YYYY-MM-DD to datetime object then to mpl format
dates = [dateutil.parser.parse(s) for s in sorted(parsed.keys())]
datempl = matplotlib.dates.date2num(dates)

# for all the tags, we plot data
for tag in tags:
    ax1.plot_date(datempl, data[tag], '.-', label=tag)

# add a legend in upper/left position
ax1.legend(loc=2)

# format x axis to YYYY-MM-DD
ax1.xaxis.set_major_formatter( matplotlib.dates.DateFormatter('%Y-%m-%d') )
# adjust for date plotting
fig1.autofmt_xdate(bottom=0.12)
# set title
ax1.set_title('Evolution of bts-link log messages (unsuccessful events)')
# add a grid
ax1.grid()
# save the figure to file
fig1.savefig('%s_graph_general.png' % latest_log)


## second figure: unconfigured

# figure much bigger (double) of default
fig2 = plt.figure(figsize=(12,8), dpi=100)
ax2 = fig2.add_subplot(111)

# for all the package to display
for i, pkg in enumerate(sorted_pkgs):
# generated an horizontal bar
    ax2.barh(i, pkg[1])

# set teh Y axis ticks and labels to package names
plt.yticks(np.arange(len(sorted_pkgs))+0.4, [pkg[0] for pkg in sorted_pkgs], fontsize='x-small')
# set the X axis to only the ttttteh values of the bars
# below a trick to only count teh values once (using dict for distinct)
ax2.set_xticks(dict.fromkeys([pkg[1] for pkg in sorted_pkgs]).keys())
# set title
ax2.set_title("Packages' Bugs with Unmatched/Unconfigure Remote BTS")
# set grid line only for X axis
# http://osdir.com/ml/python.matplotlib.general/2004-11/msg00107.html
ax2.xaxis.grid(alpha=0.5)
# save the figure to file
fig2.savefig('%s_graph_unconfig.png' % latest_log)

# show both figure
plt.show()
