#!/usr/bin/python
#
# Script to recover the summary files from the log file
# Results will be partial, but still better than nothing

from __future__ import with_statement

import glob
import os
import os.path
import datetime
import sys

for logfile in glob.glob('log_*'):
    summaryfile = logfile.replace('log_', 'summary_')
    if not os.path.isfile(summaryfile):
        print summaryfile + " recovering"
        summary = {'A': 0, 'C': 0, 'D': 0, 'E': 0, 'I': 0, 'M': 0, 'S': 0, 'T': 0, 'U': 0, 'X': 0}
        with open(logfile) as logf:
            for line in logf:
                if len(line) >= 2 and line[1] == ':':
                    summary[line[0]] += 1

        with open(summaryfile, 'w') as f:
            f.write("Execution beginning: \n" )
            f.write("Execution complete:  %s\n" % datetime.datetime.fromtimestamp(os.stat(logfile)[8]))
            f.write("Elapsed time:        %s\n\n" % (datetime.datetime.fromtimestamp(0) - datetime.datetime.fromtimestamp(0)))

            f.write("Tags summary:\n")
            for key in sorted(summary.keys()):
                f.write("  %s: %d\n" % (key, summary[key]))

