# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import sys, os, re, time, traceback, signal

makeToken_re = re.compile(r"[^a-zA-Z0-9@.+\-]")

class ParseExn(Exception):
    def __init__(self, url, reason = None):
        self.url    = url
        self.reason = reason

    def __str__(self):
        return  "Parse error: [%s] %s" % (self.url, self.reason or "")


class NoStatusExn(Exception):
    """Exception to notify a 'no status' on a remote bts link"""
    def __init__(self, url, remote):
        self.url    = url
        self.remote = remote

    def __str__(self):
        return  "No status for %s on [%s]" % (self.remote, self.url)


class DupeExn(Exception):
    def __init__(self, url):
        self.url = url

    def __str__(self):
        return  "Does not deals dupes: [%s]" % (self.url)

class ConnectionFailedExn(Exception):
    def __init__(self, remote, reason):
        self.remote = remote
        self.reason = reason
    
    def __str__(self):
        return 'Could not connect to %s: [%s]' % (self.remote, self.reason)

def maketoken(val):
    if val and val.lower() == 'none': return None
    return (val and makeToken_re.sub('-', val)) or None


def warn(s):
    print >> sys.stderr, s


def die(s):
    print >> sys.stderr, s
    os.kill(os.getpid(), signal.SIGKILL)
    sys.exit(1)


def getActions(btsbug, rbug):
    """ compares debian bug and remote bug and returns a list of debbugs commands to be performed """
    
    # commands for tags to be added or removed
    add  = []
    rem  = []
    # commands for usertags to be added or removed
    uadd = []
    urem = []

    # debbugs facts
    id            = btsbug.id
    oldrStatus     = btsbug.remoteStatus()
    oldrResolution = btsbug.remoteResolution()
    
    # remote bug facts
    rWasClosed     = rbug.bts.isClosing(oldrStatus, oldrResolution)
    rWasWontfix    = rbug.bts.isWontfix(oldrStatus, oldrResolution)

    do = []
    do.append("# remote status report for #%s (https://bugs.debian.org/%s)" % (id, id))
    do.append("# Bug title: %s" %(btsbug.subject))
    do.append("#  * %s" % (rbug.uri))
    
    # test if remote status changed
    rstatus = rbug.status
    if rstatus != oldrStatus:
        do.append("#  * remote status changed: %s -> %s" % (oldrStatus or "(?)", rstatus))
        if rstatus:
            uadd.append("status-%s" % rstatus)
        if oldrStatus:
            urem.append("status-%s" % oldrStatus)

    # test if remote resolution changed
    rresolution = rbug.resolution
    if rresolution != oldrResolution:
        do.append("#  * remote resolution changed: %s -> %s" % (oldrResolution or "(?)", rresolution or "(?)"))
        if rresolution:
            uadd.append("resolution-%s" % rresolution)
        if oldrResolution:
            urem.append("resolution-%s" % oldrResolution)

    # look if we have to change the 'fixed-upstream' tag
    if rbug.closed != rWasClosed:
        if rbug.closed:
            if 'fixed-upstream' not in btsbug.tags:
                do.append("#  * closed upstream")
                add.append('fixed-upstream')
        else:
            if 'fixed-upstream' in btsbug.tags:
                do.append("#  * reopen upstream")
                rem.append('fixed-upstream')

    # look if we have to change the 'wontfix' tag
    if rbug.wontfix != rWasWontfix:
        if rbug.wontfix:
            if 'wontfix' not in btsbug.tags:
                do.append("#  * upstream said bug is wontfix")
                add.append('upstream')
                add.append('wontfix')
        else:
            if 'wontfix' in btsbug.tags:
                do.append("#  * upstream said bug isn't wontfix anymore")
                rem.append('wontfix')

    # build mail
    res = []

    if not rbug.isForwardOK(btsbug):
        res.append("forwarded %s %s" % (id, rbug.getForward(btsbug)))
    if rem:
        res.append("tags %s - %s" % (id, ' '.join(rem)))
    if add:
        res.append("tags %s + %s" % (id, ' '.join(add)))
    if urem:
        res.append("usertags %s - %s" % (id, ' '.join(urem)))
    if uadd:
        res.append("usertags %s + %s" % (id, ' '.join(uadd)))

    if res:
        res = do + res + ['']

    return res


class RemoteReport:
    """Describes a bug report that exists in a remote bug tracker."""
    def __init__(self, data, bts):
        self.bts = bts

        assert(data.id and data.status)

        self.id         = data.id
        self.status     = maketoken(data.status)
        self.resolution = maketoken(data.resolution)
        self.uri        = self.bts.getUri(self.id)

        self.closed     = self.bts.isClosing(self.status, self.resolution)
        self.wontfix    = self.bts.isWontfix(self.status, self.resolution)

    def isForwardOK(self, btsbug):
        """Is the forwarded-to OK, or do we have to change it (duplicate, merges, etc.)"""

        oldforwardid = self.bts.extractBugid(btsbug.forward)
        # the forward didn't really change WRT ID, we were not redirected in principle
        if str(oldforwardid) == str(self.id):
            # there shouldn't have been a merge, since forward was already good
            # so return false if there used to be a merge, so that it can be removed 
            if btsbug.merge :
                return False
            # otherwise true : no need to change anything 
            
            # unless the URI would have changed changed (redirection by the BTS) and we would like to apply the change
            # but, at the moment, we prefer to be "conservative" and avoid changes that aren't strictly necessary 
            #if btsbug.forward != self.uri :
            #    return False
            #else :
            return True

        # now, the id changed, so we need to record a new merge upstream unless already recorded
        if btsbug.merge:
            oldmergeid = self.bts.extractBugid(btsbug.merge)
            return str(oldmergeid) == str(self.id)
        
        # In any other cases, need a new forwarded to (+ merge)
        return False

    def getForward(self, btsbug):
        """returns the forwarded URI, including the eventual merged-upstream case"""
        
        oldforwardid = self.bts.extractBugid(btsbug.forward)
        # if the forwarded id didn't change, always return what was there in case we would change it for bad
        if str(oldforwardid) == str(self.id):
            return btsbug.forward
            # if we were following redirects to same bug with different URI, we could :
            #return self.uri
        # otherwise, we need to record the new URI in the merged-upstream
        return "%s, merged-upstream: %s" % (btsbug.forward, self.uri)



class RemoteBts:
    """A remote bug tracker.
    
    all remote bugtracker classes will inherit from this one
    """
    
    ##########################################################################
    # class variables
    
    #: configured bugtrackers, will be initialized at launch time (see setup())
    resources = {}

    # supported bugtracker engines/types will be completed by BT classes when they will call RemoteBts.register() at initial compilation
    # contains for each type of BTS (berlios, bugzilla, gnats, launchpad, mantis, rt, savane, sourceforge, trac...) :
    #  - uri     : base URL of the bugtracker
    #  - uri-re  : matching regexp for bugs URLs : will describe how to extract the bug id/number
    #  - bugtype : ?
    #  - bts     : corresponding instance of the RemoteBts class
    _remotes  = {}

    ##########################################################################
    # static class methods

    def setup(cls, res):
        """ will initialize from all bugtrackers known in the config file """
        for k, v in res.iteritems():
            typ = v['type']
            # should be a supported type (among berlios, bugzilla, gnats, launchpad, mantis, rt, savane, sourceforge, trac...)
            if typ not in cls._remotes:
                print >> sys.stderr, "`%s' is an unknown remote BTS type" % (typ)
                sys.exit(1)
            v['bts'] = cls._remotes[typ](v)
            cls.resources[k] = v
    setup = classmethod(setup)

    def register(cls, typ, thecls):
        """ will register a BT class for a certain bugtracker """
        cls._remotes[typ] = thecls
    register = classmethod(register)

    def find(cls, uri):
        """ find the bugtracker instance for a given URL """
        uri_l = uri.lower()
        for k, v in cls.resources.iteritems():
            if 'uri-re' in v:
                # try to match both "as is" and lower case uri
                if v['uri-re'].match(uri) or v['uri-re'].match(uri_l):
                    return v['bts']
            elif uri.startswith(v['uri']) or uri_l.startswith(v['uri']):
                return v['bts']
        return None
    find = classmethod(find)

    ##########################################################################
    # public methods
     
    def __init__(self, cnf, extractRe = None, uriFmt = None, bugCls = None):
        """cnf : configuration bits for that bugtracker :
            - uri : base URI
            - uri-re : specific bug URL regexp
        extractRe : regexp for the understood bugs URLs
        uriFmt : format of the canonical bugs URLs
        bugCls : data retriever class
        """
        self._cnf    = cnf
        # will compile the regexp by substituting 'uri', for instance
        self._bugre  = extractRe and re.compile(extractRe % cnf)
        self._urifmt = uriFmt
        self._bugcls = bugCls
        self._queue  = []

    def enqueue(self, btsbug):
        self._queue.append(btsbug)

    def processQueue(self):
        """ process all bugs present in the queue and return resulting actions """ 
        res = []
        status = {'A': 0, 'C': 0, 'E': 0, 'I': 0}
        while self._queue:
            btsbug      = self._queue[0]
            self._queue = self._queue[1:]

            try:
                # remote bug
                rbug = self.getReport(btsbug.forward)
                if rbug:
                    status['C'] += 1
                    actions = getActions(btsbug, rbug)
                    if actions:
                        status['A'] += 1
                        res.append((btsbug, actions))
            except KeyboardInterrupt:
                die("*** ^C...")
            except ParseExn, e:
                status['E'] += 1
                warn("E: pkg=%s, bug=%s, msg=%s" % (btsbug.srcpackage, btsbug.id, str(e)))
            except DupeExn,  e:
                status['E'] += 1
                warn("E: pkg=%s, bug=%s, msg=%s" % (btsbug.srcpackage, btsbug.id, str(e)))
            # Connection or Authentication issue
            except ConnectionFailedExn, e:
                status['E'] += 1
                warn("E: pkg=%s, bug=%s, msg=%s" % (btsbug.srcpackage, btsbug.id, str(e)))
                # no need to keep polling the rest of the bugs on the same endpoint
                break
            except NoStatusExn,  e:
                status['I'] += 1
                warn("I: pkg=%s, bug=%s, msg=%s" % (btsbug.srcpackage, btsbug.id, str(e)))
            except:
                status['E'] += 1
                i = 0
                type, value, tb = sys.exc_info()
                exn = traceback.format_exception_only(type, value)
                warn("E: #%s [%s] raised an exception %s" % (btsbug.id, btsbug.forward, exn))
                for file, lineno, _, text in traceback.extract_tb(tb):
                    i += 1
                    warn("  %d. %-70s [%s:%s]" % (i, text, os.path.basename(file), lineno))

            if not self._queue:
                break

            time.sleep(3)
        return res, status

    def getReport(self, uri):
        data = self._getReportData(uri)
        return data and RemoteReport(data, self)

    def extractBugid(self, uri):
        """extract a bug number/id from a URI matching the 'uri-re' regexp""" 
        
        # if this particular bugtracker has a custom uri-re, apply it
        if 'uri-re' in self._cnf:
            # first we match as it is
            m = self._cnf['uri-re'].match(uri)
            if m:
                return m and m.group(1)
            else:
                # if fails, we try with lowercase
                m = self._cnf['uri-re'].match(uri.lower())
                return m and m.group(1)
        else:
            # otherwise, use the standard one for such bugtracker
            return self._extractBugid(uri)

    def getUri(self, bugId):
        if 'bugfmt' in self._cnf:
            return self._cnf['bugfmt'] % (bugId)
        return self._getUri(bugId)

    ##########################################################################
    # public virtuals to be overloaded in children classes

    def isClosing(self, status, resolution):
        """Test status for closed state""" 
        pass
    def isWontfix(self, status, resolution):
        """Test status for won't fix state""" 
        pass

    ##########################################################################
    # protected virtual methods

    def _extractBugid(self, uri):
        assert self._bugre
        # first we match as it is
        m = self._bugre.match(uri)
        if m:
            return m and m.group(1)
        else:
            # if fails, we try with lowercase
            m = self._bugre.match(uri.lower())
            return m and m.group(1)

    def _getUri(self, bugId):
        assert self._urifmt
        return self._urifmt % {'uri': self._cnf['uri'], 'id': bugId}

    def _getReportData(self, uri):
        assert self._bugcls
        return self._bugcls(uri, self.extractBugid(uri))

    def _bugId(self, data):
        return data.id

# Import all bugtracker classes which will in turn register with RemoteBts.register()
import allura, bitbucket, bugzilla, gnats, launchpad, mantis, rt, savane, sourceforge, trac, gforge, github, googlecode, roundup, misc, flyspray, jira, redmine, gitlab, prosody, debbugs
