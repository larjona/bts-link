# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   (c) 2019 Sandro Tosi <morph@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

try:
    from debianbts import debianbts
except:
    # needed to support the version in stretch, 2.6.1
    import debianbts
from __init__ import *

class DebbugsData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Debbugs: no id")
        domain = 'https://'+uri.split('/')[2]

        # FIXME: different debbugs instances can have different cgi paths
        debianbts._soap_client_kwargs['location'] = domain+'/cgi/soap.cgi'

        bug_status = debianbts.get_status(id)[0]

        self.status = bug_status.pending
        self.resolution = ' '.join(bug_status.tags)

class RemoteDebbugs(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/(?:cgi/bugreport.cgi\?bug=|)([0-9]+)$'
        urifmt = '%(uri)s/%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, DebbugsData)

    def isClosing(self, status, resolution):
        return status in ('done',)

    def isWontfix(self, status, resolution):
        if resolution:
            return 'wontfix' in resolution
        else:
            return False

RemoteBts.register('debbugs', RemoteDebbugs)
