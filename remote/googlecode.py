# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2009 Sandro Tosi <morph@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

# @see http://code.google.com/p/support/wiki/IssueTracker

import urllib, urlparse, cgi, re, ssl

from BeautifulSoup import BeautifulSoup
from __init__ import *

def parse_table(soup):

    # get the table with metainfo, identified by id=issuemeta
    cell = soup.first('td', attrs={'id' : 'issuemeta'})
    # get the first td, the span in it, then its string,
    # stripping additional newlines
    return cell.first('td').first('span').string.strip()

class GoogleCodeData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "GoogleCode: no id")

        context = ssl.create_default_context(capath=CAPATH)
        soup = BeautifulSoup(urllib.urlopen(uri, context=context))

        self.status = parse_table(soup) or failwith(uri, "GoogleCode", exn=NoStatusExn)
        self.resolution = None

        if self.status == 'Duplicate':
            raise DupeExn(uri)

class RemoteGoogleCode(RemoteBts):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, GoogleCodeData)

    # override base class method to extract the ID as useful as for _getUri
    def extractBugid(self, uri):
        if 'crbug' in uri:
            bugre = re.compile(r"http://crbug.com/(?P<id>[0-9]+)$")
            ret = bugre.match(uri).groupdict()
            return {'project': 'chromium', 'id': ret['id']}
        else:
            bugre = re.compile(r"https?://code.google.com/p/(?P<project>.*)/issues/detail\?id=(?P<id>[0-9]+)$")
            return bugre.match(uri).groupdict()

    # return a meaningful uri
    def _getUri(self, bugId):
        return "http://code.google.com/p/%(project)s/issues/detail?id=%(id)s" % bugId

    def isClosing(self, status, resolution):
        return status in ('Fixed', 'Verified', 'Archived')

    def isWontfix(self, status, resolution):
        return status in ('WontFix', 'Invalid')

RemoteBts.register('googlecode', RemoteGoogleCode)
