# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

""" Interface to remote bugzillas """

import re, sys, urllib, time, traceback, os, ssl
from __init__ import *

from base import RemoteReport, getActions, warn, die
from BeautifulSoup import BeautifulSoup

class OldBugzillaData:
    """ 'Old bugzilla' XML parser """
    
    dupre = re.compile(r'\*\*\* This (?:bug|issue) has been marked as a duplicate of(?: bug)? ([0-9]+) \*\*\*$')

    def __init__(self, uri):
        soup = BeautifulSoup(wget(uri))

        if soup.bugzilla:
            bug         = soup.bugzilla.bug     or failwith(uri, "BugZilla: no bug")
            self.id     = bug.bug_id.string     or failwith(uri, "BugZilla: no bug_id")
            self.status = bug.bug_status.string or failwith(uri, "BugZilla: no bug_status")
        elif soup.issuezilla:
            bug         = soup.issuezilla.issue   or failwith(uri, "IssueZilla: no issue")
            self.id     = bug.issue_id.string     or failwith(uri, "IssueZilla: no issue_id")
            self.status = bug.issue_status.string or failwith(uri, "IssueZilla: no issue_status")
        else:
            failwith(uri, "BugZilla: Invalid XML")

        self.resolution = (bug.resolution and bug.resolution.string) or None
        self.duplicate  = None

        if self.resolution == 'DUPLICATE':
            for t in bug.fetch('thetext')[-1::-1]:
                m   = OldBugzillaData.dupre.search(t.string)
                bug = m and m.group(1)
                if bug:
                    self.duplicate = bug
                    break

            if not self.duplicate:
                failwith(uri, "BugZilla: cannot find duplicate")

class BugzillaData:
    """ Modern bugzilla XML parser """
    
    dupre = re.compile(r'\*\*\* This (?:bug|issue) has been marked as a duplicate of(?: bug)? ([0-9]+) \*\*\*$')

    def __init__(self, bug):

        self.id     = bug.bug_id.string     or failwith("?", "BugZilla: no bug_id")
        self.status = bug.bug_status.string or failwith(self.id, "BugZilla: no bug_status")

        self.resolution = (bug.resolution and bug.resolution.string) or None
        self.duplicate  = None

        if self.resolution == 'DUPLICATE':
            if bug.dup_id:
                self.duplicate = bug.dup_id.string
                return

            for t in bug.fetch('thetext')[-1::-1]:
                m   = BugzillaData.dupre.search(t.string)
                bug = m and m.group(1)
                if bug:
                    self.duplicate = bug
                    break

            if not self.duplicate:
                failwith(self.id, "BugZilla: cannot find duplicate")

class OldRemoteBugzilla(RemoteBts):
    def __init__(self, cnf):
        bugre  = r"^%(uri)s/show_bug.cgi\?id=([0-9]+)$"
        urifmt = "%(uri)s/show_bug.cgi?id=%(id)s"
        RemoteBts.__init__(self, cnf, bugre, urifmt)

    def __getClosingStatus(self):
        if 'closing' in self._cnf:
            return self._cnf['closing']

        try:
            context = ssl.create_default_context(capath=CAPATH)
            config = urllib.urlopen(self._cnf['uri'] + '/config.cgi', context=context)
        except (ssl.CertificateError, IOError) as e:
            # let's try not to verify that certificate at all
            context = ssl.create_default_context()
            context.check_hostname = False
            context.verify_mode = ssl.CERT_NONE
            config = urllib.urlopen(self._cnf['uri'] + '/config.cgi', context=context)

        for l in config.readlines():
            if l.startswith('var status_closed'):
                s = l[l.find('['):].strip('[] ,;\r\t\n')
                self._cnf['closing'] = [x.strip("' ") for x in s.split(',')]
                config.close()
                return self._cnf['closing']


    def isClosing(self, status, resolution):
        return resolution != 'WONTFIX' and status in self.__getClosingStatus()

    def isWontfix(self, status, resolution):
        return resolution == 'WONTFIX'

    def _getApiUri(self, bugId):
        return "%s/xml.cgi?id=%s" % (self._cnf['uri'], bugId)

    def _getReportData(self, uri):
        id = self.extractBugid(uri)
        if not id: return None

        uri = self._getApiUri(id)
        
        data = OldBugzillaData(uri)
        while data.resolution == 'DUPLICATE':
            uri  = "%s/xml.cgi?id=%s" % (self._cnf['uri'], data.duplicate)
            data = OldBugzillaData(uri)

        return data

class RemoteBugzilla(OldRemoteBugzilla):
    
    api_fields = ["bug_id", "bug_status", "resolution", "dup_id", "long_desc"]
    
    def __init__(self, cnf):
        OldRemoteBugzilla.__init__(self, cnf)

    def _getApiUri(self, bugId):
        qs  = "ctype=xml&field=" + "&field=".join(self.api_fields)
        qs += '&id=%s' % bugId
        return '%s/show_bug.cgi?%s' % (self._cnf['uri'], qs)

    def enqueue(self, btsbug):
        id = self.extractBugid(btsbug.forward)
        if id is None:
            failwith(btsbug.forward, "BugZilla: cannot extract id from fwd: %s" % (btsbug.forward))
        self._queue.append((btsbug, id))

    def processQueue(self):
        res = []
        status = {'A': 0, 'C': 0, 'E': 0, 'I': 0}
        while self._queue:
            if len(self._queue) > 100:
                subq = self._queue[0:100]
                self._queue = self._queue[100:]
            else:
                subq = self._queue
                self._queue = []

            qs  = "ctype=xml&field=" + "&field=".join(self.api_fields)
            qs += '&id=' + '&id='.join([x[1] for x in subq])
            massuri = "%s/show_bug.cgi?%s" % (self._cnf['uri'], qs)

            assoc = {}
            for v, k in subq: assoc[k] = v

            bugs = BeautifulSoup(wget(massuri))
            if not bugs:
                status['E'] += 1
                warn("E: Bugzilla: invalid XML")
                warn("             %s" % (massuri))
                continue

            for bug in bugs.bugzilla.fetch('bug'):
                try:
                    data = BugzillaData(bug)
                    if not data: continue

                    status['C'] += 1

                    rbug = RemoteReport(data, self)
                    if rbug.resolution == 'DUPLICATE':
                        self._queue.append((assoc[rbug.id], data.duplicate))
                        continue

                    actions = getActions(assoc[rbug.id], rbug)
                    if actions:
                        status['A'] += 1
                        res.append((assoc[rbug.id], actions))
                except KeyboardInterrupt:
                    die("*** ^C...")
                except ParseExn, e:
                    status['E'] += 1
                    warn("E: parse error: %s" % (str(e)))
                except DupeExn, e:
                    status['E'] += 1
                    warn("E: does not deal with dupes: %s" %(str(e)))
                except:
                    status['E'] += 1
                    i = 0
                    type, value, tb = sys.exc_info()
                    exn = traceback.format_exception_only(type, value)
                    warn("E: Bugzilla massive processing for %s raised an exception %s" % (massuri, exn))
                    for file, lineno, _, text in traceback.extract_tb(tb):
                        i += 1
                        warn("  %d. %-70s [%s:%s]" % (i, text, os.path.basename(file), lineno))

            if not self._queue:
                break

            # yes sleep a lot, we go fast
            time.sleep(20)
        return res, status

RemoteBts.register('old-bugzilla', OldRemoteBugzilla)
RemoteBts.register('bugzilla', RemoteBugzilla)
