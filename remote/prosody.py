# https://hg.prosody.im/issue-tracker/file/tip/doc/api.markdown
import urllib2, urlparse, cgi, re, json, ssl
from __init__ import *

class ProsodyIssueData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "ProsodyIssues: no id")

        context = ssl.create_default_context(capath=CAPATH)

        req = urllib2.Request(uri, headers = { 'Accept': 'application/json' })
        data = json.load(urllib2.urlopen(req, context=context))

        issue = data['issue'] or failwith(uri, "ProsodyIssues", exn=NoStatusExn)

        # status is either open or closed
        self.status = issue['state']
        self.resolution = None

        # everything else is based on tags and configurable per issue tracker instance
        if self.status == 'closed':
            # prosody uses status-* tags for status
            self.resolution = issue['_auto']['status']

            if self.resolution == "Duplicate":
                raise DupeExn(uri)

class RemoteProsodyIssues(RemoteBts):
    """
    RemoteProsodyIssues({'uri':'https://issues.prosody.im/'})
    """

    def __init__(self, cnf, extractRe = None, uriFmt = None):
        if extractRe is None:
            extractRe = r"/(?P<id>[0-9]+)$"
        if uriFmt is None:
            uriFmt = r"%(uri)s/%(id)s"
        RemoteBts.__init__(self, cnf, extractRe, uriFmt, ProsodyIssueData)

    # status is either open or closed
    def isClosing(self, status, resolution):
        return status in ('closed',)

    # Open
    #   New NeedInfo Accepted Blocked Started
    # Closed becasue done
    #   Fixed Verified Done
    # Closed because problems
    #   Invalid Duplicate WontFix CantFix CantReproduce
    def isWontfix(self, status, resolution):
        return resolution in ('WontFix', 'Invalid')

RemoteBts.register('prosody', RemoteProsodyIssues)
