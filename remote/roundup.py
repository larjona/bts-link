# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2009 Sandro Tosi <morph@debian.org>
#
# License:
#   Public Domain
###############################################################################

# @see 

# TODO: handle duplicate, usually resolution=duplicate
# TODO: enable XMLRPC interface, but upstream needs version >= 1.4.8

# *** Doc ***
#
# bugs url: http://server/path/issueNNNN
# base url: http://server/path
#
# We are lucky Roundup allows 2 native querying methods:
#
#  * CSV export (the one currently used)
#  * XML RPC
#
# Roundup allows for a lot of customization, in particular for status
# and resolution values. In order to get those values, surf to
#
# status: <base url>/status
# resolution: <base url>/resolution
#
# the same thing done by getStatus() and getResolution() method below.
#
# Anyhow, we need to visit those pages because we have to populate
# 'closing' and 'wontfix' config values.
#
# Examples urls:
#
#   * issue url     : http://bugs.python.org/issue764437
#   * base uri      : http://bugs.python.org
#   * status url    : http://bugs.python.org/status
#   * resolution url: http://bugs.python.org/resolution
#
# How to write configuration entry:
#
#  * uri must NOT have tailing /
#  * write the 'closing' and 'wontfix' item following the above
#    suggestions

import urllib, urlparse, cgi, re, ssl
from __init__ import *

class RoundupData:
    def __init__(self, uri, id):
        # to uri is the real 'uri' with '/issueNNNNN' added, we need "real uri" 
        uri_re = re.compile(r'(.*)/issue(.*)')
        real_uri = uri_re.match(uri).group(1)

        self.id = id or failwith(uri, "Roundup: no id")

        self.status = self.getStatus(real_uri, id) or failwith(uri, "Roundup", exn=NoStatusExn)
        self.resolution = self.getResolution(real_uri, id)

        # NOT SUPPORTED YET
        #if self.status == 'Duplicate':
        #    raise DupeExn(uri)

    def getStatus(self, uri, id):
        """get the issue status in two steps: get the numerical status from the issue page, then match that id against the <id, status name> list in $uri/status page"""
        # we now obtain the issue status, as a numerical value
        status_tpl = [('@action', 'export_csv'), ('@filter', 'id'), ('id', id), ('@columns', 'status')]
        status_url = '/%s?%s' % ('issue', urllib.urlencode(status_tpl))
        context = ssl.create_default_context(capath=CAPATH)
        status_content = urllib.urlopen(uri + status_url, context=context).read().split('\r\n')
        # then retrive <id, status name> list from $uri/status
        statuslist_tpl = [('@action', 'export_csv'), ('@columns', 'id,name')]
        statuslist_url = '/%s?%s' % ('status', urllib.urlencode(statuslist_tpl))
        statuslist_map = urllib.urlopen(uri + statuslist_url, context=context).read().split('\r\n')
        statuslist_dict = {}
        # let's make a dict out of this list: keys are numerical id, values are status names
        # we strip first ('id,name') and last ('\n') values from the map
        for statusitem in statuslist_map[1:len(statuslist_map)-1]:
            s = statusitem.split(',')
            statuslist_dict[s[0]] = s[1]
        # if it has 3 items, we have data (we hope)
        if len(status_content) == 3:
            return statuslist_dict[status_content[1]]
        else:
            return None
        

    def getResolution(self, uri, id):
        """get the issue resolution value in two steps: get the numerical resolution from the issue page, then match that id against the <id, resolution name> list in $uri/resolution page"""
        # we now obtain the issue resolution, as a numerical value
        resolv_tpl = [('@action', 'export_csv'), ('@filter', 'id'), ('id', id), ('@columns', 'resolution')]
        resolv_url = '/%s?%s' % ('issue', urllib.urlencode(resolv_tpl))
        context = ssl.create_default_context(capath=CAPATH)
        resolv_content = urllib.urlopen(uri + resolv_url, context=context).read().split('\r\n')
        # then retrive <id, status name> list from $uri/resolution - IF IT EXISTS!!
        resolvlist_tpl = [('@action', 'export_csv'), ('@columns', 'id,name')]
        resolvlist_url = '/%s?%s' % ('resolution', urllib.urlencode(resolvlist_tpl))
        resolvlist_map = urllib.urlopen(uri + resolvlist_url, context=context).read().split('\r\n')
        resolvlist_dict = {}
        # let's make a dict out of this list: keys are numerical id, values are resolution names
        # we strip first ('id,name') and last ('\n') values from the map
        for resolvitem in resolvlist_map[1:len(resolvlist_map)-1]:
            r = resolvitem.split(',')
            resolvlist_dict[r[0]] = r[1]
        # if it has 3 items, we have data (we hope)
        if len(resolv_content) == 3 and resolv_content[1] != 'None':
            return resolvlist_dict[resolv_content[1]]
        else:
            return None

        pass

class RemoteRoundup(RemoteBts):
    def __init__(self, cnf):
        # save to an object attribute
        self.cnf = cnf

        bugre  = r"^%(uri)s/issue([0-9]+)$"
        urifmt = "%(uri)s/issue%(id)s"
        RemoteBts.__init__(self, cnf, bugre, urifmt, RoundupData)

    def _getClosingStatus(self):
        """get the config values for closing state"""
        if 'closing' in self.cnf:
            return self.cnf['closing']
        else:
            return [None]

    def _getWontfixStatus(self):
        """get the config value for wontfix state"""
        if 'wontfix' in self.cnf:
            return self.cnf['wontfix']
        else:
            return [None]

    def isClosing(self, status, resolution):
        return status in self._getClosingStatus()

    def isWontfix(self, status, resolution):
        return (status in [self._getWontfixStatus()]) \
            or (resolution in [self._getWontfixStatus()])


RemoteBts.register('roundup', RemoteRoundup)
