# vim:set encoding=utf-8:

# Copyright:
#   © 2009 Sanghyeon Seo <sanxiyn@gmail.com>
#   © 2018 Sandro Tosi <morph@debian.org>
#
# License:
#   Public Domain

# @see https://developer.atlassian.com/server/jira/platform/rest-apis/
# @see https://docs.atlassian.com/software/jira/docs/api/REST/7.10.2/#api/2/issue-getIssue
# @see https://developer.atlassian.com/server/jira/platform/basic-authentication/

# Some Jira instances require username/password to access their REST APIs.
# register a user on the instance and add it to remote/secrets.py

import urllib2, urlparse, cgi, re, json, ssl

from __init__ import *
from base import die
from secrets import SECRETS
import base64

class JIRAData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Jira: no id")

        # get username and password to access the Jira instance, if needed
        token = None
        for k, v in SECRETS['jira'].items():
            if uri.startswith(k):
                if v:
                    token = base64.encodestring(v).strip()
                else:
                    # no auth
                    token = -1
                break
        if not token:
            failwith(uri, "Jira: no auth token defined")

        context = ssl.create_default_context(capath=CAPATH)
        # if there is no auth required, dont set the Authorization header
        if token == -1:
            req = urllib2.Request(uri.replace('/browse/', '/rest/api/latest/issue/'))
        else:
            req = urllib2.Request(uri.replace('/browse/', '/rest/api/latest/issue/'), headers = { 'Authorization': 'Basic %s' % token })
        data = json.load(urllib2.urlopen(req, context=context))

        self.status = data['fields']['status']['name']
        self.resolution = None

        # resolution can be empty if the issue is still open
        if 'resolution' in data['fields'] and data['fields']['resolution']:
            self.resolution = data['fields']['resolution']['name']
            if self.resolution == 'Duplicate':
                raise DupeExn(uri)

class RemoteJIRA(RemoteBts):
    def __init__(self, cnf):
        bugre = r'^%(uri)s/browse/([A-Z0-9]+-[0-9]+)$'
        urifmt = '%(uri)s/browse/%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, JIRAData)

    def isClosing(self, status, resolution):
        return status in ('Resolved', 'Closed')

    def isWontfix(self, status, resolution):
        return resolution in ("Won't Fix",)

RemoteJIRA.register('jira', RemoteJIRA)
